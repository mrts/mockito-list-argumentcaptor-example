package com.example;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ArgumentCaptorTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private CalculateSum calculateSum;

    @InjectMocks
    private Calculator calculator;

    @Test
    public void argumentCaptorWorks() {
        final List<Integer> integers = Arrays.asList(1, 2, 3);
        when(calculateSum.sum(integers)).thenReturn(6);
        @SuppressWarnings("unchecked")
        final ArgumentCaptor<List<Integer>> argumentCaptor = ArgumentCaptor.forClass(List.class);

        final int sum = calculator.sum(integers);
        assertThat(sum, is(equalTo(6)));
        verify(calculateSum).sum(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue(), is(equalTo(integers)));
    }

}
