package com.example;

import java.util.List;

public class Calculator {

    private CalculateSum calculateSum;

    Calculator(CalculateSum calculateSum) {
        this.calculateSum = calculateSum;
    }

    int sum(List<Integer> numbers) {
        return calculateSum.sum(numbers);
    }

}
