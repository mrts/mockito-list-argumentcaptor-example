package com.example;

import java.util.List;

public interface CalculateSum {

    int sum(List<Integer> numbers);

}
