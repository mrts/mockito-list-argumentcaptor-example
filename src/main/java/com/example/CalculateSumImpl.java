package com.example;

import java.util.List;

public class CalculateSumImpl {

    public int sum(List<Integer> numbers) {
        return numbers.stream().mapToInt(Integer::intValue).sum();
    }

}
